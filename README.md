# About

This is a VK music chart parser. It saves current chart in .csv format and commits updated data to a separate continuous table.

# Frameworks

The script is based on Twill and (TBA) Selenium. Twill is based on Py2.7 and is not compatible with Py3 at all (even with use of 2to3 refractory routine). Twill provides a convenient CLI-based browsing API. However, it does not support JavaScript, so only 50 first records are available to grab.
Selenium is expected to be a better substitution of Twill, since it supports JS and other cool stuff (it renders a page with JSON chart it its code).
The script is based on mobile version of VK (https://m.vk.com) and, in a Twill&lxml-based implementation, just raw html parsing is fulfilled.
