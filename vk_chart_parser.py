# coding=utf-8
import twill.commands as tw_comm
from lxml import html
import pandas
import sqlite3, sqlalchemy
from datetime import date
import os, sys, logging, re
# +cssselect, urllib3, requests
#don't forget about unicode in 177th line of twill's browser.py

# exogeneous variables
VK_LOGIN = 'alexdobr1991@mail.ru'
VK_PASS = 'mulali73'
SQLITE3_DB_FILENAME = ''  # or leave it empty to use 'vk_chart.db' inside script's fol (also consider :memory:)
SECS_BETWEEN_VK_REQS = 3
CSV_CHARTS_DIR = ''  # leave empty to use 'charts' folder inside script's directory
FULL_TABLE_DIR = '' # leave empty to use
FULL_TABLE_NAME = ''  # leave empty to use 'vk_chart_table.csv' filename for comprehensive chart
ENABLE_LOGGING = True


# routines

def get_current_vk_chart_twill(login, pwd, sleeptime=3):
    # type: (str, str, int) -> pandas.DataFrame
    """
    This function gets VK login and password (and optionally, sleep time between requests)
    and returns numpy/pandas DataFrame object with current VK music chart.
    twill package used here, so no JS is supported.
    """
    # login page
    tw_comm.go("https://m.vk.com")
    # tw_comm.showforms()
    # tw_comm.formclear("1")
    tw_comm.formvalue("1", "email", login)
    tw_comm.formvalue("1", "pass", pwd)
    tw_comm.submit("1")
    tw_comm.sleep(sleeptime)

    # after login navigation
    tw_comm.go(r"https://m.vk.com/audio?act=recoms")
    # tw_comm.showlinks()
    # tw_comm.follow('42')
    sys.stdout = open(os.devnull, 'w')
    tw_comm.go("https://m.vk.com" + tw_comm.showlinks()[42][1])
    sys.stdout = sys.__stdout__
    tw_comm.sleep(sleeptime)

    # chart page [with volatile uri]
    # sys.stdout = open(os.devnull, 'w')
    html_data = tw_comm.show()
    # sys.stdout = sys.__stdout__

    # lxml magic goes here
    tree = html.fromstring(html_data)
    artists = tree.xpath('//*[@id]/div[1]/div[3]/div[2]/span[3]')
    titles = tree.xpath('//*[@id]/div[1]/div[3]/div[2]/span[1]')
    artworks = tree.xpath(
        '/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/div[1]/div[@id]/div[1]/div[1]/@style')
    lengths = tree.xpath(
        '/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[2]/div[1]/div[@id]/div[1]/div[3]/div[1]/@data-dur')

    # arrange and return data
    current_date = date.today()
    pos_date = 'pos_{}_{}_{}'.format(current_date.day, current_date.month, current_date.year)
    df = pandas.DataFrame([{'id': (artists[i].text + "-" + titles[i].text),
                            'artist': artists[i].text,
                            'title': titles[i].text,
                            'artwork': artworks[i].replace("background-image:url(", "").replace(")", ""),
                            'length': lengths[i],
                            pos_date: (i + 1)} for i in range(len(titles))])
    df = df[['id', 'artist', 'title', 'artwork', 'length', pos_date]]
    return df


def write_chart_to_csv(df_chart, csv_charts_dir):
    # type: (pandas.DataFrame, str) -> int
    """
    This subroutine writes .csv with current pandas.DataFrame chart to certain dir.
    """
    # endogenous variables
    script_path = os.path.dirname(sys.argv[0])  # path of script location
    today = date.today()  # date as date object (.day, .month, .year consts are available)
    # write a .csv with current chart
    # if there is a blank path var (default path is used)
    if csv_charts_dir == '':
        csv_charts_dir = script_path + '/charts/'
        logging.info('Default path ({}) is chosen.'.format(csv_charts_dir))
    # if the path var is not a directory (default path is used)
    if not (os.path.isdir(csv_charts_dir)):
        csv_charts_dir = script_path + '/charts/'
        logging.error('You have entered a path that is not a directory. A default path will be chosen instead.')
        logging.info('Default path ({}) is chosen.'.format(csv_charts_dir))
    # if path does not exist (it is created recursively)
    if not (os.path.exists(csv_charts_dir)):
        os.makedirs(csv_charts_dir)
        logging.info('Path {} did not exist, so it was created.'.format(csv_charts_dir))
    # finally, write a file
    try:
        csv_filename = '{}/chart_{}_{}_{}.csv'.format(csv_charts_dir, today.day, today.month, today.year)
        df_chart.to_csv(csv_filename, encoding='utf-8', index=False)
    except Exception as e:
        logging.fatal('There are problems with .csv file writing. Check path availability and ownership.')
        logging.fatal('Exception: {}'.format(e.message))
        return 1
    else:
        logging.info('The chart was successfully written to {}.'.format(csv_filename))
        return 0


def make_full_table_path(csv_full_table_filename, csv_full_table_dir):
    # type: (str, str) -> str
    script_path = os.path.dirname(sys.argv[0])  # path of script location
    # write a .csv with current chart
    # if there is a blank filename var (default filename is used)
    if csv_full_table_filename == '':
        csv_full_table_filename = 'vk_chart_table.csv'
    # if there is a blank path var (default path is used)
    if csv_full_table_dir == '':
        csv_full_table_dir = script_path + '/charts/'
        logging.info('Default path ({}) is chosen.'.format(csv_full_table_dir))
    # if the path var is not a directory (default path is used)
    if not (os.path.isdir(csv_full_table_dir)):
        csv_full_table_dir = script_path + '/charts/'
        logging.error('You have entered a path that is not a directory. A default path will be chosen instead.')
        logging.info('Default path ({}) is chosen.'.format(csv_full_table_dir))
    # if path does not exist (it is created recursively)
    if not (os.path.exists(csv_full_table_dir)):
        os.makedirs(csv_full_table_dir)
        logging.info('Path {} did not exist, so it was created.'.format(csv_full_table_dir))
    # finally, return a path
    return '{}{}'.format(csv_full_table_dir, csv_full_table_filename)


def get_full_table_from_csv(path):
    # type: (str) -> pandas.DataFrame
    # read
    if not (os.path.isfile(path)):  # does file exist?
        logging.error('A .csv file with full table is not found at "{}". '.format(path))
        return pandas.DataFrame()
    else:
        try:  # is file a readable DataFrame?
            last_full_table = pandas.read_csv(path, index_col=False, encoding='utf-8')
        except:
            logging.fatal('A .csv file with full table at {} cannot be read. '.format(path) +
                          'Check if it is corrupted.')
            return pandas.DataFrame()
        else: # concatenate
            print('A .csv file with full table was successfully read.')
            return last_full_table


def combine_current_and_full_charts(current_chart, last_full_chart):
    # type: (pandas.DataFrame, pandas.DataFrame) -> pandas.DataFrame
    print type(last_full_chart)
    if last_full_chart.shape == (0,0):
        return current_chart
    try:  # can last and current DataFrames be merged?
        current_chart.set_index('id')
        last_full_chart.set_index('id')
        #last_full_chart = last_full_chart.drop(columns=['Unnamed: 0'])  # drop annoying index column
        updated_full_chart = pandas.concat([last_full_chart, current_chart], axis=1, join='outer', sort=False)
        updated_full_chart = updated_full_chart.loc[:, ~updated_full_chart.columns.duplicated()]  # drop dup columns
    except:
        logging.fatal('Concatenation of last full chart and current chart has been failed.')
    else:
        return updated_full_chart
    # TODO: sort columns in full chart


def write_full_table_to_csv(new_full_table, path):
    # type: (pandas.DataFrame, str) -> int
    try:
        new_full_table.to_csv(path, encoding='utf-8', index=False)
    except Exception as e:
        logging.fatal('There are problems with .csv file writing. Check path availability and ownership.')
        logging.fatal('Exception: {}'.format(e.message))
        return 1
    else:
        logging.info('An updated continuous chart was successfully written to {}.'.format(path))
        return 0

# DEPRECATED
def _write_full_table_to_csv(df_current_chart, csv_full_table_dir, csv_full_table_filename):
    """
    This subroutine writes .csv with continuous table with every track and its chart pos.
    :param df_current_chart: pandas.DataFrame
    :param csv_full_table_dir: str
    :param csv_full_table_filename: str
    """

    # endogenous variables
    script_path = os.path.dirname(sys.argv[0])  # path of script location

    # write a .csv with current chart
    # if there is a blank filename var (default filename is used)
    if csv_full_table_filename == '':
        csv_full_table_filename = 'vk_chart_table.csv'
    # if there is a blank path var (default path is used)
    if csv_full_table_dir == '':
        csv_full_table_dir = script_path + '/charts/'
        logging.info('Default path ({}) is chosen.'.format(csv_full_table_dir))
    # if the path var is not a directory (default path is used)
    if not (os.path.isdir(csv_full_table_dir)):
        csv_full_table_dir = script_path + '/charts/'
        logging.error('You have entered a path that is not a directory. A default path will be chosen instead.')
        logging.info('Default path ({}) is chosen.'.format(csv_full_table_dir))
    # if path does not exist (it is created recursively)
    if not (os.path.exists(csv_full_table_dir)):
        os.makedirs(csv_full_table_dir)
        logging.info('Path {} did not exist, so it was created.'.format(csv_full_table_dir))

    # finally, read and write a file
    csv_path = '{}{}'.format(csv_full_table_dir, csv_full_table_filename)
    # read
    if not(os.path.isfile(csv_path)):  # does file exist?
        logging.error('A .csv file with continuous table is not found at "{}". '.format(csv_path) +
                      'A new file will be created instead.')
        df_to_write = df_current_chart
    else:
        try:  # is file readdable DataFrame?
            last_full_table = pandas.read_csv(csv_path, index_col=False)
        except:
            logging.fatal('A .csv file with continuous table at {} cannot be read. '.format(csv_path) +
                          'Check if it is corrupted.')
            return 1
        else:
            # concatenate
            try:  # can last and current DataFrames be merged?
                df_current_chart.set_index('id')
                last_full_table.set_index('id')
                last_full_table = last_full_table.drop(columns=['Unnamed: 0']) # drop annoying index column
                df_to_write = pandas.concat([last_full_table, df_current_chart], axis=1, join='outer', sort=False)
                df_to_write = df_to_write.loc[:, ~df_to_write.columns.duplicated()] # drop dup columns
            except:
                logging.fatal('Concatenation of last continuous chart and current chart has been failed.')
                return 1
    # write
    try:
        df_to_write.to_csv(csv_path, encoding='utf-8', index=False)
    except Exception as e:
        logging.fatal('There are problems with .csv file writing. Check path availability and ownership.')
        logging.fatal('Exception: {}'.format(e.message))
        return 1
    else:
        logging.info('An updated continuous chart was successfully written to {}.'.format(csv_path))
        return df_to_write


def sqlite3_write_new_data(database, df_chart, full_table, table_date):
    # type: (str, pandas.DataFrame, pandas.DataFrame, date.today()) -> int
    """
    This function submits new data to sqlite3 db table.
    vkchart:(id text, artist text, title text, artwork text, length int)
    :param database: string
    :param df_chart: pandas.DataFrame: id, artist, title, artwork, length, pos_{date(-)}
    :param table_date: datetime.date
    :return: void
    """
    today = [table_date.day, table_date.month, table_date.year]
    # check out name of database file
    if database == '':
        script_path = os.path.dirname(sys.argv[0])
        database = '/' + script_path + '/vk_chart.db'
    elif database == ':memory:':
        database = ''  # it is weird, but required for sqlalchemy sqlite3 memory db instance

    # fire up engine
    engine = sqlalchemy.create_engine("sqlite://{}".format(database), echo=False)

    # create a table with current chart and csv full chart
    df_chart.to_sql(name='chart_{}_{}_{}'.format(today[0],today[1],today[2]), con=engine,
                    if_exists='replace', index=False, index_label='id')
    full_table.to_sql('full_chart', con=engine, if_exists='replace', index=False, method=None, index_label='id')
    return 0


if __name__ == '__main__':
    # check if logging is required
    if ENABLE_LOGGING:
        logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                            level=logging.DEBUG, filename=u'vkchart_log.log',
                            filemode='a+')  # sets up logger
    logging.info('')
    logging.info('-' * 80)
    logging.info('The script has been launched.')

    # get chart in pandas.DataFrame format
    current_chart = get_current_vk_chart_twill(login=VK_LOGIN, pwd=VK_PASS, sleeptime=SECS_BETWEEN_VK_REQS)
    try:
        pass # current_chart = get_current_vk_chart_twill(login=VK_LOGIN, pwd=VK_PASS, sleeptime=SECS_BETWEEN_VK_REQS)
    except Exception as e:
        logging.fatal('There was an error with fetching a chart from VK. ' +
                      'Check page availability, its code structure, or Internet connection.')
        logging.fatal('Exception: {}'.format(e))
        logging.fatal('Object: {}'.format(e.args[1]))
        sys.exit(1)
    else:
        logging.info('The chart page was successfully fetched.')
        logging.info('The first row is:\n.'.format(current_chart.head(1)))  # TODO: print first row of current chart

    # write charts to csv
    # current_chart
    write_chart_to_csv(df_chart=current_chart, csv_charts_dir=CSV_CHARTS_DIR)
    # full_chart
    # compile path
    full_chart_path = make_full_table_path(FULL_TABLE_NAME, FULL_TABLE_DIR)
    # get last full table
    full_table = get_full_table_from_csv(full_chart_path)
    # update it
    updated_full_table = combine_current_and_full_charts(current_chart, full_table)
    # write updated full table
    write_full_table_to_csv(updated_full_table, full_chart_path)

    # SQL subroutine
    logging.info("SQL operations have been started...")
    try:
        sqlite3_write_new_data(SQLITE3_DB_FILENAME, current_chart, updated_full_table, date.today())
    except Exception as e:
        logging.fatal('There was an error while working with SQL')
        logging.fatal('Exception: {}'.format(e))
    else:
        logging.info('SQL operations succeeded.')

    logging.info('Turning off!')